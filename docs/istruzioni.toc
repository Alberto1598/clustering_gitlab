\contentsline {section}{\numberline {1}Scopo del documento}{2}{section.1}%
\contentsline {section}{\numberline {2}Requisiti}{2}{section.2}%
\contentsline {subsection}{\numberline {2.1}Tool richiesti}{2}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Struttura del progetto}{2}{subsection.2.2}%
\contentsline {section}{\numberline {3}Installazione}{2}{section.3}%
\contentsline {section}{\numberline {4}Configurazione}{3}{section.4}%
\contentsline {subsection}{\numberline {4.1}File di configurazione}{3}{subsection.4.1}%
\contentsline {subsubsection}{\numberline {4.1.1}config.properties}{3}{subsubsection.4.1.1}%
\contentsline {subsubsection}{\numberline {4.1.2}sonar-project.properties}{3}{subsubsection.4.1.2}%
\contentsline {subsection}{\numberline {4.2}Primo avvio}{4}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}Jenkins}{4}{subsubsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2}Sonarqube}{6}{subsubsection.4.2.2}%
\contentsline {paragraph}{Premessa}{6}{section*.2}%
\contentsline {subsection}{\numberline {4.3}Integrare Jenkins con Sonarqube}{7}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}Creare un nuovo progetto}{8}{subsection.4.4}%
\contentsline {subsubsection}{\numberline {4.4.1}Progetto semplice}{9}{subsubsection.4.4.1}%
\contentsline {subsubsection}{\numberline {4.4.2}Integrare Jenkins con Gitlab}{10}{subsubsection.4.4.2}%
