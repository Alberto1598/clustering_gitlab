\documentclass[../static_and_dynamic_analysis_tools.tex]{subfiles}

\begin{document}

\subsection{\texttt{Cprofile} e \texttt{Profile}}
\label{sub:cprofile_e_profile}

La Standard Library del linguaggio Python offre i tool \texttt{Cprofile} e \texttt{Profile} per svolgere profiling del codice. Fino alla release 2.5 del linguaggio era disponibile un terzo tool denominato \texttt{Hotshot}. Tale tool, scritto in C, è stato realizzato allo scopo di minimizzare i tempi di overhead causati dagli altri strumenti di profiling del codice ma richiedeva lunghi tempi di esecuzione. Per quest'ultimo motivo nelle versioni successive alla 2.5 il modulo è stato deprecato. 
Gli strumenti tutt'ora offerti dal linguaggio rimangono quindi \texttt{Cprofile} e \texttt{Profile} che svolgono profiling determistico del codice e costituiscono due implementazioni della stessa interfaccia.
Le principali differenze dei due moduli sono dovute prevalentemente a dettagli implementativi:
\begin{itemize}
	\item \texttt{Cprofile} è il modulo raccomandato dal linguaggio adatto per la maggior parte degli utenti. È un'estensione scritta in C adatta per eseguire il profiling di programmi di lunghezza considerevole.
	\item \texttt{Profile} è un modulo scritto interamente in Python che aggiunge un significativo overhead ai programmi di cui svolge profiling. È, tuttavia, consigliato in caso si voglia estendere le funzionalità di un profiler preeesistente.
\end{itemize} 

Entrambi i moduli misurano la quantità di tempo che ogni funzione impiega a runtime rieseguendo più volte il codice sorgente. Producono, inoltre, dei report che è possibile personalizzare utilizzando la libreria \texttt{pstats} offerta dal linguaggio.
D'ora in poi considereremo negli esempi il modulo \texttt{Cprofile} in quanto \texttt{Profile} offre esattamente le stesse funzioni.

\subsubsection{Installazione e avvio}
\label{subs:installazione_e_avvio}
Per utilizzare Cprofile è necessario :
\begin{itemize}
	\item Importare il modulo opportuno tramite \texttt{import Cprofile}.
	\item Utilizzare la funzione \texttt{Cprofile.run('foo()')} all'interno di uno script Python. Con \texttt{foo()} si intende una generica funzione.
\end{itemize}

Per eseguire il profiling di un intero script direttamente da linea di comando è possibile utilizzare la seguente istruzione:

\begin{minted}{bash}
	
	python -m cProfile [-o output_file] [-s sort_order] (-m module | myscript.py)
\end{minted}

dove:
\begin{itemize}
	\item Il flag -o serve per specificare il file in cui stampare i risultati della profilazione. In caso non venga specificato nulla 
	i risultati verranno stampati su standard output.
	\item Il flag -s specifica come ordinare i risultati. Tale flag si applica solo nel caso in cui non sia specificato -o.
	\item Il flag -m serve a specificare che si sta svolgendo profiling di un modulo specifico(e.g una libreria) e non di uno script.
\end{itemize}

Un esempio di output dato da \texttt{Cprofile} è il seguente :
\begin{figure}[ht]
	\centering
	\includegraphics[width=9cm]{components/img/Cprofile-output.png}
	\caption{esempio di output di \texttt{Cprofile}}
\end{figure}

Nell'immagine si può osservare come \texttt{Cprofile} conti il numero totale di chiamate a funzione. Le chiamate primitive sono quelle che non sono state chiamate per ricorsione.
È inoltre indicato il tempo impiegato.
È presente poi una tabella che mostra dettagli relativi alle funzioni chiamate :
\begin{itemize}
	\item \textbf{ncalls} indica il numero totale di chiamate alla funzione specifica. Se ncalls è nella forma x/y significa che c'è stata della ricorsione. x è il numero totale di volte in cui la funzione è stata chiamata, mentre y indica il numero di volte in cui la funzione primitiva è stata chiamata(cioè non per ricorsione).
	\item \textbf{tottime} indica con precisione al millesimo il tempo speso nella funzione indicata(escluse le funzioni che quest'ultima ha invocato).
	\item \textbf{percall} è il quoziente di tottime diviso per ncalls.
	\item \textbf{cumtime} rappresenta per ogni funzione la somma di tottime e del tempo speso nelle sue sottofunzioni.
	\item \textbf{percall} è il quoziente di cumtime diviso il numero di chiamate a funzione primitive.
	\item \textbf{filename:lineno(function)} contiene informazioni relative a ciascuna funzione.
\end{itemize}

\subsubsection{Editor grafici}
\label{subs:editor_grafici}

Esistono una serie di librerie che rendono possibile visualizzare graficamente i report di Cprofile. Tra queste troviamo :
\begin{itemize}
	\item Pycallgraph.
	\item Graph2Dot.
	\item SnakeViz.
\end{itemize}
Pycallgraph e Graph2Dot necessitano l'installazione  di Graphviz un tool open-source che consente di disegnare grafi scritti in linguaggio DOT.
\subsubsection{Punti di forza e debolezza}
\label{subs:punti_di_forza_e_debolezza}

Segue una tabella che illustra i principali pregi e difetti di \texttt{Profile} e \texttt{Cprofile}:

\begin{table}[H]
\centering
\begin{tabular}{|>{\raggedright\arraybackslash}m{80mm}|m{80mm}|}
\hline
\multicolumn{1}{|>{\centering\arraybackslash}m{80mm}|}{\textbf{Punti di forza}} 
    & \multicolumn{1}{>{\centering\arraybackslash}m{80mm}|}{\textbf{Punti di debolezza}} \\
\hline
\begin{enumerate}
	\item Forniscono un report molto dettagliato che permette di ricavare numerose informazioni relative a ogni funzione contenuta nel programma.
	\item Sono altamente personalizzabili e permettono di misurare diverse statistiche ( e.g. il numero di context switch volontari, eventuali memory leak).
\end{enumerate} &
\begin{enumerate}
	\item Sono eseguiti sul medesimo processo del programma di cui si sta effettuando il profiling. Ciò provoca un pesante overhead che influisce sulle performance del profiler stesso causando errori e distorsioni nei risultati prodotti.
	\item Misurano il CPU-time(il tempo in cui è effettivamente impiegata la CPU dal processo ) e non il cosiddetto wall-clock-time(il tempo che è effettivamente passato da quando il processo è in esecuzione). Un processo che svolge poca computazione, ma resta in attesa di dati da molti servizi può produrre un report di profilazione soggetto a molti errori.
	\item Sono in grado di misurare la velocità di esecuzione di una funzione, ma non riescono a stabilire per quali input la funzione venga effettivamente rallentata.
\end{enumerate} \\
\hline
\end{tabular}
\caption{Punti di forza e di debolezza \texttt{Profile} e \texttt{Cprofile}}
\end{table}


\subsection{Pyinstrument}
\label{sub:pyinstrument}

PyInstrument è un profiler scritto in Python e di terze parti, rilasciato con BSD-3-Clause-License che supporta leversioni 2.7 e 3.3+ del linguaggio. PyInstrument è un profiler statistico che non misura il tempo di esecuzione di ogni funzione contenuta nel programma di cui si sta eseguendo il profiling. Il suo compito, invece, è quello di registrare il call stack dei processi in esecuzione ogni 1ms. Sebbene 1ms possa sembrare un intervallo di tempo non sufficente per avere un risultato soddisfacente è necessario considerare che il tool è ottimizzato in modo che nel caso in cui una singola funzione venisse eseguita per un lungo intervallo di tempo allora ne registrebbe comunque il call stack al termine della sua esecuzione.

\subsubsection{Installazione e avvio}
\label{subs:installazione_e_avvio}

PyInstrument può essere installato utilizzando il package manager di python denominato pip. Per installarlo basta lanciare da un terminale il comando :
\begin{minted} {bash}
	pip install pyinstrument
\end{minted}
Come per Cprofile, PyInstrument può essere invocato sia direttamente dal codice del programma di cui si vuole eseguire profiling sia da terminale specificando il nome dello script di cui si vuole effettuare profilazione.
Per fare uso di PyInstrument all'interno di un programma è necessario :
\begin{itemize}
	\item Importare Profiler dalla libreria pyinstrument tramite l'istruzione \texttt{from pyinstrument import Profiler}.
	\item Istanziare il Profiler con l'istruzione \texttt{profiler = Profiler()}.
	\item Strutturare il codice nel seguente modo :
	\begin{minted}{python}
		profiler = Profiler()
		profiler.start()
		# codice di cui si vuole effettuare il profiling
		profiler.stop()
	\end{minted}
	\item È possibile stampare il risultato della misurazione con l'istruzione
	\begin{minted}{bash}
		print(profiler.output_text(unicode=True, color=True))
	\end{minted}
\end{itemize}
Per eseguire pyinstrument su uno script python basta eseguire il comando :

\begin{minted}{bash}

	pyinstrument script.py
\end{minted}
Aggiungendo il flag -r html è possibile visualizzare un report in formato html.

\subsubsection{Punti di forza e debolezza}
\label{subs:punti_di_forza_e_debolezza}
Segue una tabella che illustra i principali punti di forza e di debolezza di PyInstrument:

\begin{table}[H]
\centering
\begin{tabular}{|>{\raggedright\arraybackslash}m{80mm}|m{80mm}|}
\hline
\multicolumn{1}{|>{\centering\arraybackslash}m{80mm}|}{\textbf{Punti di forza}} 
    & \multicolumn{1}{>{\centering\arraybackslash}m{80mm}|}{\textbf{Punti di debolezza}} \\
\hline
\begin{enumerate}
	\item A differenza di Cprofile, essendo un profiler statistico ha un overhead molto minore.
	\item Misura il wall-clock time e non il CPU-time.
\end{enumerate} &
\begin{enumerate}
	\item Poco personalizzabile.
	\item È un tool molto giovane.
	\item Non è un strumento built-in del linguaggio.
\end{enumerate} \\
\hline
\end{tabular}
\caption{Punti di forza e di debolezza di PyInstrument}
\end{table}


\subsection{Pyspy}
\label{pyspy}

Pyspy è un profiler di terze parti scritto in Rust e distribuito con licenza MIT. È il successore di Pyflame, un progetto ormai deprecato che non supportava Python 3.7 e i sistemi operativi OSX, Windows e FreeBSD.

\subsubsection{Installazione e avvio}
\label{subs:installazione_e_avvio}
Pyspy può essere installato tramite il package manager pip con la seguente istruzione :

\begin{minted}{bash}

	pip install py-spy
\end{minted}

Per ARM e FreeBSD è necessario scaricare i binari dal link \url{https://github.com/benfred/py-spy/releases}.
È possibile scaricare py-spy anche attraverso cargo, il package manager di Rust. In tal caso basterà utilizzare l'istruzione :

\begin{minted}{bash}

	cargo install py-spy
\end{minted}
Pyspy legge direttamente la memoria del programma di cui si sta effettuando profiling. A tale scopo esegue delle chiamate di sistema direttamente al sistema operativo :
\begin{itemize}
	\item Nei sistemi Linux utilizza la system call process\_vm\_readv.
	\item Nei sistemi OSX utilizza la system call vm\_read.
	\item Nei sistemi Windows utilizza la system call ReadProcessMemory.
\end{itemize}
Più nello specifico si occupa di leggere il valore della variabile globale PyInterpreterState dell'interprete in modo da ricavare quali siano i thread attualmente attivi. Successivamente legge il call stack di ogni thread. \\
Pyspy consente di eseguire diversi comandi. La sintassi standard è la seguente:
\begin{minted}{bash}

py-spy {nome-comando} {--pid |-- python3} {{identificativo del processo} | {script.py}}
\end{minted}
Ogni comando può essere lanciato mediante l'identificativo di processo anteponendo \texttt{--pid} oppure utilizzando il nome dello script con il prefisso \texttt{-- python3}.
I principali comandi che Pyspy rende disponibili sono :
\begin{itemize}
	\item \texttt{top} che permette di visualizzare quali funzioni stanno impiegando il maggior numero di risorse all'interno del programma.
	\item \texttt{dump} che permette di visualizzare il call stack per ogni thread attualmente attivo.
	\item \texttt{report} che consente di generare un report in formato grafico che mostra i risultati.
\end{itemize}
Aggiungendo il flag --locals è possibile visualizzare le variabili locali associate ad ogni call stack.

\subsubsection{Punti di forza e debolezza}
\label{subs:punti_di_forza_e_debolezza}
Segue una tabella che illustra i principali punti di forza e di debolezza di Pyspy:

\begin{table}[H]
\centering
\begin{tabular}{|>{\raggedright\arraybackslash}m{80mm}|m{80mm}|}
\hline
\multicolumn{1}{|>{\centering\arraybackslash}m{80mm}|}{\textbf{Punti di forza}} 
    & \multicolumn{1}{>{\centering\arraybackslash}m{80mm}|}{\textbf{Punti di debolezza}} \\
\hline
\begin{enumerate}
	\item Overhead molto basso e ottime performance dovute al fatto che come in Pyflame il profiler esegue su un processo distinto che quindi non rallenta l'esecuzione del programma su cui si sta effettuando profiling.
	\item Data la sua efficienza può essere utilizzato anche in produzione.
	\item Permette di osservare il call stack di ogni thread attualmente attivo. È possibile inoltre visualizzare il valore di ogni variabile nel rispettivo call stack.
\end{enumerate} &
\begin{enumerate}
	\item La System integrity protection di OSX può impedire ai processi di leggere lo stato della memoria in particolare dei binari presenti in \textit{/usr/bin}. Potrebbe essere richiesto installare una diversa distribuzione Python.
	\item Non presenta un'API che è possibile utilizzare direttamente all'interno dei programmi.
\end{enumerate} \\
\hline
\end{tabular}
\caption{Punti di forza e di debolezza di Pyspy}
\end{table}


\end{document}


