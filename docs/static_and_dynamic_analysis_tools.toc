\contentsline {section}{\numberline {1}Scopo del documento}{2}{section.1}%
\contentsline {section}{\numberline {2}Introduzione}{2}{section.2}%
\contentsline {subsection}{\numberline {2.1}Analisi statica del codice}{2}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Analisi dinamica del codice}{2}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Profiling}{2}{subsubsection.2.2.1}%
\contentsline {paragraph}{Profiling deterministico o ad eventi}{3}{section*.2}%
\contentsline {paragraph}{Profiling statistico}{3}{section*.3}%
\contentsline {section}{\numberline {3}Strumenti di analisi statica}{3}{section.3}%
\contentsline {subsection}{\numberline {3.1}Pylint}{3}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Installazione e avvio}{3}{subsubsection.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.2}Punti di forza e debolezza}{4}{subsubsection.3.1.2}%
\contentsline {subsection}{\numberline {3.2}Pyflakes}{4}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Installazione e avvio}{4}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}Punti di forza e debolezza}{5}{subsubsection.3.2.2}%
\contentsline {subsection}{\numberline {3.3}Bandit}{5}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Coverage.py}{5}{subsection.3.4}%
\contentsline {subsubsection}{\numberline {3.4.1}Punti di forza e debolezza}{6}{subsubsection.3.4.1}%
\contentsline {subsection}{\numberline {3.5}Sonarqube}{6}{subsection.3.5}%
\contentsline {subsubsection}{\numberline {3.5.1}Installazione di una demo}{7}{subsubsection.3.5.1}%
\contentsline {subsubsection}{\numberline {3.5.2}Installazione di un server per la produzione}{7}{subsubsection.3.5.2}%
\contentsline {subsubsection}{\numberline {3.5.3}Punti di forza e debolezza}{8}{subsubsection.3.5.3}%
\contentsline {section}{\numberline {4}Strumenti di analisi dinamica}{8}{section.4}%
\contentsline {subsection}{\numberline {4.1}\texttt {Cprofile} e \texttt {Profile}}{8}{subsection.4.1}%
\contentsline {subsubsection}{\numberline {4.1.1}Installazione e avvio}{9}{subsubsection.4.1.1}%
\contentsline {subsubsection}{\numberline {4.1.2}Editor grafici}{10}{subsubsection.4.1.2}%
\contentsline {subsubsection}{\numberline {4.1.3}Punti di forza e debolezza}{10}{subsubsection.4.1.3}%
\contentsline {subsection}{\numberline {4.2}Pyinstrument}{10}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}Installazione e avvio}{10}{subsubsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2}Punti di forza e debolezza}{11}{subsubsection.4.2.2}%
\contentsline {subsection}{\numberline {4.3}Pyspy}{11}{subsection.4.3}%
\contentsline {subsubsection}{\numberline {4.3.1}Installazione e avvio}{11}{subsubsection.4.3.1}%
\contentsline {subsubsection}{\numberline {4.3.2}Punti di forza e debolezza}{12}{subsubsection.4.3.2}%
