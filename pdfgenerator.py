from PIL import Image
from fpdf import FPDF
from PyPDF2 import PdfFileMerger, PdfFileReader, PdfFileWriter
from weasyprint import HTML, CSS
import fileinput
import calendar
import time
import datetime
import pkg_resources
import configparser

def add_to_pdf(pdf, filename, width, height):
	file = open (filename, "r")
	for i in file :
		pdf.cell(width, height, txt = i, ln = 1)

def print_title(pdf, text, size, align):
	pdf.set_font("Arial", "BU", size)
	pdf.cell(align, 8, txt = text, ln = 1, align = "C")
	pdf.set_font("Times", size=10)

def create_image_pdfs():
	pdfImage = FPDF("L", "mm", "A3")
	HTML("scalene-report.html").write_pdf("scalene-profiler.pdf",  stylesheets=[CSS(string='@page {size: 15in 12in;}')])
	#pdfImage.add_page()
	#pdfImage.image("Cprofile-graph.png",  0, 20, 400, 400)
	image = Image.open("Cprofile-graph.png")
	im = image.convert('RGB')
	im.save("Cprofile.pdf")
	pdfReader = PdfFileReader("Cprofile.pdf")

	pdfImage.add_page()
	print_title(pdfImage, "MEMORY PROFILER", 15, 400)
	pdfImage.image("memory-profiler.png", 15, 20, 400, 200)
	pdfImage.output("graphs.pdf")

def create_report_pdf(author, project_name):
	pdf = FPDF()
	pdf.add_page()
	print_title(pdf, "PIPELINE REPORT", 20, 200)
	ts = calendar.timegm(time.gmtime())
	timestamp = datetime.datetime.fromtimestamp(ts).strftime("%d %B %Y, %H:%M:%S")
	pdf.write(5, "Date : " + timestamp + "\n")
	pdf.write(5, "Author : " + author + "\n")
	pdf.write(5, "Project name : " + project_name +"\n")
	pdf.write(5, "Sonarqube link : http://localhost:9000/")
	pdf.write(5, "\n")
	pdf.accept_page_break()
	print_title(pdf, "ANALISI DINAMICA DEL CODICE", 17, 200)
	print_title (pdf ,"CPROFILE REPORT", 15, 200)
	add_to_pdf(pdf, "cProfile-report.txt", 200, 5)
	pdf.add_page('L')
	print_title(pdf ,"ANALISI STATICA DEL CODICE", 17, 300)
	print_title (pdf ,"PYLINT REPORT", 15, 300)
	add_to_pdf(pdf, "pylint-report.txt", 200, 5)
	pdf.add_page()
	print_title(pdf, "BANDIT REPORT", 15, 200)
	add_to_pdf(pdf, "bandit-report.txt", 200, 5)
	pdf.output("rep.pdf")



config = configparser.ConfigParser()
config.read("config.properties")
project_name = config.get("ProjectData", "ProjectName")
author = config.get("ProjectData", "Author")
create_report_pdf(author, project_name)
create_image_pdfs()
merger = PdfFileMerger()
merger.append("rep.pdf")
merger.merge(2, "Cprofile.pdf")
merger.merge(3, "graphs.pdf")
merger.merge(4, "scalene-profiler.pdf")
merger.write("report.pdf")
merger.close()





