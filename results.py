import pstats
results = pstats.Stats('profile.pstats').strip_dirs()
with open('cProfile-report.txt', 'w') as stream :
	pOutput = pstats.Stats('profile.pstats', stream = stream).strip_dirs()
	pOutput.sort_stats('tottime').print_stats(20)
	pOutput.sort_stats('cumtime').print_stats(20)

results.dump_stats('graph.pstats')
