from setuptools import setup, find_packages
from codecs import open
from os import path
from glob import glob

__version__ = '0.1.0'

here = path.abspath(path.dirname(__file__))

# get the dependencies and installs
with open(path.join(here, 'requirements.txt'), encoding='utf-8') as f:
    all_reqs = f.read().split('\n')

install_requires = [x.strip() for x in all_reqs if 'git+' not in x]
dependency_links = [x.strip().replace('git+', '') for x in all_reqs if x.startswith('git+')]

setup(
    name='clustering_algorithms',
    version=__version__,
    description='Dbscan and K-means comparison',
    url='https://gitlab.com/Alberto1598/clustering_gitlab',
    download_url='https://gitlab.com/Alberto1598/clustering_gitlab/-/tree/master/',
    license='GPL',
    include_package_data=True,
    author='Alberto Cocco',
    install_requires=install_requires,
    setup_requires=['numpy>=1.10', 'scipy>=0.17'],
    data_files=[
	('dataset', glob('dataset/*.csv')),
	('conf.ini')
    ],
    dependency_links=dependency_links,
    author_email='albertococco98@gmail.com',
    packages=find_packages(exclude=['docs'])
)
