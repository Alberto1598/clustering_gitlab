from sklearn.cluster import DBSCAN
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import normalize
import sys
import numpy as np
import pandas as pd 
import sklearn.datasets as dataset
import configparser
import matplotlib.pyplot as plt

def db_scan():
	# get sys arg from command line
	dataset = sys.argv[1]
	config = configparser.ConfigParser()
	config.read('conf.ini')
	eps = float(config[dataset]['dbscan_eps'])
	# read dataset
	X = pd.read_csv(config[dataset]['path'], delimiter = config[dataset]['delimiter'], header = int(config[dataset]['header']))
	X = X.iloc[:,int(config[dataset]['iloc1']):int(config[dataset]['iloc2'])].values
	# scale data
	scaler = StandardScaler()
	X_scaled =  scaler.fit_transform(X)
	# use Dbscan in order to cluster data
	clustering = DBSCAN(eps=eps, min_samples=int(config[dataset]['dbscan_minPts'])).fit(X_scaled)
	n_cluster = len(set(clustering.labels_))-(1 if -1 in clustering.labels_ else 0)
	n_noise = list(clustering.labels_).count(-1)
	print(n_cluster)
	print(n_noise)
	core_samples_mask = np.zeros_like(clustering.labels_, dtype=bool)
	core_samples_mask[clustering.core_sample_indices_] = True
	labels = clustering.labels_
	unique_labels = set(labels)
	colors = [plt.cm.Spectral(each)
	for each in np.linspace(0, 1, len(unique_labels))]
	for k, col in zip(unique_labels, colors):
		 if k == -1:
			# Black used for noise.
		    col = [0, 0, 0, 1]
		 class_member_mask = (labels == k)
		 xy = X[class_member_mask & core_samples_mask]
		 plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
			     markeredgecolor='black', markersize=5)
		 xy = X[class_member_mask & ~core_samples_mask]
		 plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
			     markeredgecolor='black', markersize=2)

	plt.title(' dbscan results with eps value : %.2f' % eps)
	plt.savefig( 'dbscan_graph.png')

if __name__ == '__main__':
	db_scan()

