from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import normalize
import configparser
import numpy as np
import pandas as pd 
import sklearn.datasets as dataset
import matplotlib.pyplot as plt
import sys


def k_means():
	# get sys arg from command line
	dataset = sys.argv[1]
	config = configparser.ConfigParser()
	config.read('conf.ini')
	# read dataset
	X = pd.read_csv(config[dataset]['path'], delimiter = config[dataset]['delimiter'], header = int(config[dataset]['header']))
	X = X.iloc[:,int(config[dataset]['iloc1']):int(config[dataset]['iloc2'])].values
	print(X)
	# scale data
	scaler = StandardScaler()
	X_scaled =  scaler.fit_transform(X)
	# use k-means in order to cluster data
	clustering = KMeans(n_clusters = int(config[dataset]['k_means_k'])).fit(X_scaled)
	labels = clustering.labels_
	centroids = clustering.cluster_centers_
	centroids_array = np.array(centroids)
	print(centroids_array)
	unique_labels = set(labels)
	colors = [plt.cm.Spectral(each)
	          for each in np.linspace(0, 1, len(unique_labels))]
	for k, col in zip(unique_labels, colors):
	    class_member_mask = (labels == k)

	    xy = X[class_member_mask]
	    plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col), markeredgecolor='black', markersize=5)
	    

	plt.scatter(centroids_array[:,0], centroids_array[:,1], 80, marker="x", color='k')

	plt.title("k-means results")
	plt.savefig("k_means_graph.png")
	
if __name__ == '__main__':
	k_means()

